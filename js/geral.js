(function(){

	$('header .menu-redes-sociais span.participar a.assinar-newsletter').click(function(){
		$('.newsletter').addClass('formulario-newletter-ativo');
		setTimeout(function(){
			$('body').addClass('travar-scroll');
		}, 300);
	});

	$('.newsletter .formulario-newsletter span').click(function(){
		$('.newsletter').removeClass('formulario-newletter-ativo');
		$('body').removeClass('travar-scroll');
	});

	$('header .abrir-menu-mobile').click(function(){
		$('header nav.menu-principal').addClass('menu-mobile');
		setTimeout(function(){
			$('body').addClass('travar-scroll');
		}, 300);
	});

	$('header nav.menu-principal span.close-menu').click(function(){
		$('header nav.menu-principal').removeClass('menu-mobile');
		setTimeout(function(){
			$('body').removeClass('travar-scroll');
		}, 300);
	});

	$('header .menu-redes-sociais span.pesquisar-icone').click(function(){
		$('header .formulario-pesquisar').addClass('pesquisar-ativo');
		setTimeout(function(){
			$('body').addClass('travar-scroll');
		}, 300);
	});

	$('header .formulario-pesquisar .close-pesquisar').click(function(){
		$('header .formulario-pesquisar').removeClass('pesquisar-ativo');
		$('body').removeClass('travar-scroll');
	});

}());